package com.example.TestProject.repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.TestProject.model.User;

public interface UserRepo extends JpaRepository<User, Integer> {

	@Modifying
	@Query("update User u set u.userCompany = ?1 where u.userId = ?2")
	@Transactional
	int updateUser(String company, int id);

	
	
}
