package com.example.TestProject.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



@Entity
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;
	private String userName;
	private String userContact;
	private String userCompany;
	private String userPincode;
	private String userGender;
	private boolean userExperience;
	
	
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public User(int userId, String userName, String userContact, String userCompany, String userPincode,
			String userGender, boolean userExperience) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userContact = userContact;
		this.userCompany = userCompany;
		this.userPincode = userPincode;
		this.userGender = userGender;
		this.userExperience = userExperience;
	}


	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserContact() {
		return userContact;
	}
	public void setUserContact(String userContact) {
		this.userContact = userContact;
	}
	public String getUserCompany() {
		return userCompany;
	}
	public void setUserCompany(String userCompany) {
		this.userCompany = userCompany;
	}
	public String getUserPincode() {
		return userPincode;
	}
	public void setUserPincode(String userPincode) {
		this.userPincode = userPincode;
	}
	public String getUserGender() {
		return userGender;
	}
	public void setUserGender(String userGender) {
		this.userGender = userGender;
	}
	public boolean isUserExperience() {
		return userExperience;
	}
	public void setUserExperience(boolean userExperience) {
		this.userExperience = userExperience;
	}


	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", userContact=" + userContact + ", userCompany="
				+ userCompany + ", userPincode=" + userPincode + ", userGender=" + userGender + ", userExperience="
				+ userExperience + "]";
	}
	
	
	
	
}