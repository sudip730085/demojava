package com.example.TestProject.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.TestProject.model.User;
import com.example.TestProject.repo.UserRepo;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepo userRepo;
	@Override
	public boolean saveUser(User user) {
		
		userRepo.save(user);
			return true;	
	}
	


	public boolean deleteById(int id) {
		userRepo.deleteById(id);
		return true;
		
	}



	public Optional<User> getUserById(int id) {
		
		return userRepo.findById(id);
	}



	public boolean updateUser(String company,int id) {
		if( userRepo.updateUser(company,id)==1) {
		 return true;
		}
		else {
			return false;
		}
	}



}
