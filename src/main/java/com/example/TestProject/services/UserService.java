package com.example.TestProject.services;

import com.example.TestProject.model.User;

public interface UserService {

    boolean saveUser(User user);
	
    boolean deleteById(int id);
}
